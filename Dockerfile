FROM node:alpine3.14

WORKDIR /code

ENV PORT 8080

COPY package.json /code/package.json

RUN npm install

COPY . ./code

RUN ls

CMD [ "node", "code/src/index.js" ]

EXPOSE 8080


